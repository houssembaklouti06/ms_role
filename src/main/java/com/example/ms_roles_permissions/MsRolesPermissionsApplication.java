package com.example.ms_roles_permissions;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsRolesPermissionsApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsRolesPermissionsApplication.class, args);
    }

}
